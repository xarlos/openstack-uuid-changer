#!/bin/bash
#
# This is a VERY hacky way to change a UUID for an instance for openstack.

# Reuired arguments:
#   $1 == current uuid
#   $2 == new uuid

# Method:
#   1) Search the uuid from the xml dumps from the instances from $(virsh list --all)
#   2a From this output, grab a copy of the xml dump, and make the modifications of the UUID
#   2b (virsh) undefine the current server-nnnnnnn that we have taken the dump from
#   2c (virsh) re-define with the new uuid
#   3a dump the entire database dbbackup.txt
#   3b sed out the old uuid for the new uuid
#   3c reimport the database
#   4 Pray.


######################################################################################
# Check the passed arguments are valid
######################################################################################

if [ $# -ne 2 ] ; then
   echo "Please pass <CURRENT uuid> <NEW uuid> in your argument"
   exit 1
fi

CURRENT_UUID=$1
NEW_UUID=$2


######################################################################################
# Iterate the instances (with virsh) and see if we can find the instance uuid that
# was supplied in the passed arguments
######################################################################################

found_uuid=
found_name=

IFS=$'\n'
for instance in $(virsh list --name --uuid --inactive); do
  # echo ${instance}
  instance_uuid=$( echo ${instance} | awk '{print $1}' )
  instance_name=$( echo ${instance} | awk '{print $2}' )

  if [ ${instance_uuid} == ${CURRENT_UUID} ]; then 
    found_uuid=${instance_uuid}
    found_name=${instance_name}
  fi

done

if [ ${found_uuid} ] && [ ${found_name} ] ; then
  echo "Found server: ${found_name} with uuid: ${found_uuid}"
else
  echo "No server found. Is the VM turned off?"
  exit 1
fi



######################################################################################
# After checking the instance exists, we need to dump out the XML
# eg: virsh dumpxml instance-00000003 > test-01.xml
######################################################################################

virsh dumpxml ${found_name} > ./${found_name}-backup.xml
cp ./${found_name}-backup.xml ./${found_name}-new.xml
sed -i "s/${found_uuid}/${NEW_UUID}/g" ./${found_name}-new.xml
virsh undefine ${found_name} 
virsh define ./${found_name}-new.xml


######################################################################################
# Dump the database and modify the uuid
######################################################################################
FILEDATE=$(date +%Y%m%d-%H:%M:%S)

mysqldump --all-databases > ./db_${FILEDATE}.sql
cp ./db_${FILEDATE}.sql ./db_new_${FILEDATE}.sql
sed -i "s/${found_uuid}/${NEW_UUID}/g" ./db_new_${FILEDATE}.sql
mysql < ./db_new_${FILEDATE}.sql
